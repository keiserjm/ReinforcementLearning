import matplotlib as mpl
import matplotlib.pyplot as plt
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout

def draw_network(genome,config,show_disabled,seed,node_names):

    G = nx.DiGraph()
    #
    inputs = set()
    for n in config.genome_config.input_keys:
        inputs.add(n)
        G.add_node(node_names[n])
    #
    outputs = set()
    for n in config.genome_config.output_keys:
        outputs.add(n)
        G.add_node(node_names[n])
    #
    used_nodes = set(genome.nodes.keys())
    #
    for n in used_nodes:
        if n in inputs or n in outputs:
            continue
        G.add_node(str(n))
    #
    # Build entire graph
    EnEdges,EnColor,EnLabel = [],[],{}
    DiEdges,DiColor,DiLabel = [],[],{}
    for cg in genome.connections.values():
        inp,out = cg.key
        v0,v1 = node_names.get(inp,str(inp)),node_names.get(out,str(out))
        G.add_edge(v0,v1)
        if cg.enabled:
            EnEdges.append((v0,v1))
            EnColor.append(cg.weight)
            EnLabel[(v0,v1)] = '{0:6.3f}'.format(cg.weight)
        elif show_disabled:
            DiEdges.append((v0,v1))
            DiColor.append(cg.weight)
            DiLabel[(v0,v1)] = '{0:6.3f}'.format(cg.weight)

    # Make layout
    pos = nx.spring_layout(G, seed=seed)
    pos = graphviz_layout(G, prog='dot', args="-Grankdir=LR")

    fig,axs = plt.subplots(figsize=(10,5))
    nodes = nx.draw_networkx_nodes(G, pos) # , node_size=node_sizes, node_color="indigo")
    labels = nx.draw_networkx_labels(G,pos,font_weight='bold')

    cmap = plt.cm.plasma
    for eStyle,eList,eColor,eLabel in zip(['solid','dashed'],[EnEdges,DiEdges],[EnColor,DiColor],[EnLabel,DiLabel]):
        edges = nx.draw_networkx_edges(
            G,
            pos,
            # node_size=node_sizes,
            arrowstyle="->",
            arrowsize=30,
            style=eStyle,
            edge_color=eColor,
            edgelist = eList,
            edge_cmap=cmap,
            width=3,
            # connectionstyle='arc3, rad = 0.1',
        )
        nx.draw_networkx_edge_labels(
            G, pos,
            edge_labels=eLabel,
            font_size=15,
            font_color='black',
            font_weight='bold',
        )

    pc = mpl.collections.PatchCollection(edges)
    axs.set_axis_off()
    plt.show()

    return